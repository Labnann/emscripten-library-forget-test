LTCOMPILE.o = libtool --mode=compile --tag=CC emcc
LTLINK.o = libtool --mode=link --tag=CC emcc

all: hello2.so hello.so main

src/main.o: CC = $(LTCOMPILE.o)

main: LDFLAGS = -g -sMAIN_MODULE -sFETCH -pthread
main: CFLAGS = -g
main: CC = $(LTLINK.o)
main: src/main.o
	$(LINK.o) -o $@ $(^:%.o=%.lo) hello.so hello2.so

src/hello2.o: CC = $(LTCOMPILE.o)
src/hello.o: CC = $(LTCOMPILE.o)

hello.so: LDFLAGS = -shared -sSIDE_MODULE=2 -sEXPORTED_FUNCTIONS=@symfile -pthread
hello.so: CFLAGS = -fPIC -g
hello.so: CC = $(LTLINK.o)
hello.so: src/hello.o
	$(LINK.o) -o $@ $(^:%.o=%.lo) hello2.so

hello2.so: LDFLAGS = -shared -sSIDE_MODULE=2 -sEXPORTED_FUNCTIONS=@symfile -pthread
hello2.so: CFLAGS = -fPIC -g
hello2.so: CC = $(LTLINK.o)
hello2.so: src/hello2.o
	$(LINK.o) -o $@ $(^:%.o=%.lo)


clean:
	rm -f main hello.so hello2.so src/*.lo src/*.o src/*.so main.wasm main.out \
	main.html main.js main.worker.js  src/hello2.o
